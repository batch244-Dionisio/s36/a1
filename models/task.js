// Schema - blueprints 
const mongoose = require("mongoose")

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});
// model names should be capitalized and singular form
module.exports = mongoose.model("Task", taskSchema);