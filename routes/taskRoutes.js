// routes  - contains all endpoint for our applications

const express = require("express")
const router = express.Router()

const taskController = require("../controllers/taskController");

//route to all get the tasks
router.get("/", (req, res ) => {
	taskController.getAllTasks().then(resultFromController => res.send(
		resultFromController));
});

// route to create a new task 
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(
		resultFromController));
})

// delete
// ":id" wild card 
router.delete("/:id", (req,	res) => {
		   taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// update
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})



// activity 

// get specific task
router.get("/:id", (req, res ) => {
	taskController.specificTasks(req.params.id).then(resultFromController => res.send(
		resultFromController));
})


// update to "complete"
router.put("/:id/complete", (req, res) => {
	taskController.updateToComplete(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


// export the module
module.exports = router;