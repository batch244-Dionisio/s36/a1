// controllers - business logic
const Task = require("../models/task");

// controller for getting all task
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	});
};

// creating tasks
module.exports.createTask = (requestBody) => {

	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
}

// delete task

module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		} else {
			return removedTask;
		}
	})
}

// update

module.exports.updateTask = (taskId, newContent) => { 

	return Task.findById(taskId).then((result, error) => {
		if (error){
			console.log(error);
			return false;
		} else {
			result.name = newContent.name;

			return result.save().then((updatedTask, saveErr) => {

				if(saveErr){
					console.log(saveErr)
					return false;
				} else{
					return updatedTask;
				}
			})
		}
	}) 
} 

// activity 

module.exports.specificTasks = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	});
};

// update to "complete"

module.exports.updateToComplete = (taskId) => { 

	return Task.findById(taskId).then((result, error) => {
		if (error){
			console.log(error);
			return false;
		} else {
			result.status = "complete";

			return result.save().then((updatedTask, saveErr) => {

				if(saveErr){
					console.log(saveErr)
					return false;
				} else{
					return updatedTask;
				}
			})
		}
	}) 
}